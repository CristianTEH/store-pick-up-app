const configs = {
  baseURL: "http://localhost:9001/api",
  url: {
    orders: "orders",
  },
  cacheTime: 10,
};
export default configs;
