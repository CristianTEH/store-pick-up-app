import { createSlice } from "@reduxjs/toolkit";
import { createSelector } from "reselect";
import { apiCallBegan } from "./api";
import moment from "moment";
import configs from "./config";

const { url, cacheTime } = configs;

const slice = createSlice({
  name: "orders",
  initialState: {
    list: [],
    loading: false,
    lastFetch: null,
  },
  reducers: {
    /**
     * Triggers when orders have been returned
     *
     * @param {array} orders
     * @param {object} action
     */
    ordersRequested: (orders, action) => {
      orders.loading = true;
      orders.success = false;
    },
    /**
     * Triggers when request failes
     *
     * @param {array} orders
     * @param {object} action
     */
    ordersRequestFailed: (orders, action) => {
      orders.loading = false;
      orders.success = false;
    },
    /**
     * Triggers when orders have been recieved
     *
     * @param {array} orders
     * @param {object} action
     */
    ordersReceived: (orders, action) => {
      orders.list = action.payload;
      orders.loading = false;
      orders.lastFetch = Date.now();
    },
    /**
     * Triggers when a new order has been made
     *
     * @param {array} orders
     * @param {object} action
     */
    orderAdded: (orders, action) => {
      orders.list.push(action.payload);
      orders.success = true;
    },
    /**
     * Resets order
     *
     * @param {array} orders
     * @param {object} action
     */
    ordersReseted: (orders, action) => {
      orders.success = false;
    },
  },
});

const {
  ordersRequested,
  ordersRequestFailed,
  ordersReceived,
  ordersReseted,
  orderAdded,
} = slice.actions;
export default slice.reducer;

// Action Creators
/**
 * Loads orders from server
 *
 * @returns {function(*, *): *}
 */
export const loadOrders = () => (dispatch, getState) => {
  const { lastFetch } = getState().entities.orders;
  const diffInMinutes = moment().diff(moment(lastFetch), "minutes");
  dispatch(ordersReseted());
  if (diffInMinutes < cacheTime) {
    return;
  }
  dispatch(
    apiCallBegan({
      url: url.orders,
      onStart: ordersRequested.type,
      onSuccess: ordersReceived.type,
      onError: ordersRequestFailed.type,
    })
  );
};

/**
 * Adds new order
 *
 * @param {object} order
 */
export const addOrder = (order) =>
  apiCallBegan({
    url: url.orders,
    method: "post",
    data: order,
    onSuccess: orderAdded.type,
  });

// Selector
export const getOrders = createSelector(
  (state) => state.entities.orders,
  (orders) => {
    return orders.list;
  }
);
