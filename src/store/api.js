import { createAction } from "@reduxjs/toolkit";

export const apiCallBegan = createAction("api/callBegan");
export const apiCallSucces = createAction("api/callSucces");
export const apiCallFailed = createAction("api/callFailed");
