import axios from "axios";
import configs from "../config";
import * as actions from "../api";

/**
 * Handles all requests
 *
 * @param {function} dispatch
 * @returns {function(*): Function}
 */
const api = ({ dispatch }) => (next) => async (action) => {
  if (action.type !== actions.apiCallBegan.type) return next(action);

  const { url, method, data, onStart, onSuccess, onError } = action.payload;
  const { baseURL } = configs;

  if (onStart) dispatch({ type: onStart });

  next(action);

  try {
    const response = await axios.request({
      baseURL,
      url,
      method,
      data,
    });
    dispatch(actions.apiCallSucces(response.data));
    if (onSuccess) dispatch({ type: onSuccess, payload: response.data });
  } catch (error) {
    dispatch(actions.apiCallFailed(error.message));
    if (onError) dispatch({ type: onError, payload: error.message });
  }
};

export default api;
