import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { Link } from "react-router-dom";
import { useStylesHeader } from "./Styles";

export default function Header() {
  const classes = useStylesHeader();
  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.appBar}>
        <Toolbar>
          <Link to="/">
            <img src="/box-pick-up.png" alt="Logo" className={classes.logo} />
          </Link>
          <Typography className={classes.title}>PickApp</Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
}
