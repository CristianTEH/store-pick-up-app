import React from "react";
import { Link } from "react-router-dom";
import { Button } from "@material-ui/core";
import { useStylesButtons } from "./Styles";

function LinkButton({ location, text, color }) {
  const classes = useStylesButtons();
  return (
    <Link to={location} className={classes.linkButton}>
      <Button variant="contained" color={color}>
        {text}
      </Button>
    </Link>
  );
}

export default LinkButton;
