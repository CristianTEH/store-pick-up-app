import { makeStyles } from "@material-ui/core";
import { createMuiTheme } from "@material-ui/core/styles";

// Theme
const palette = {
  primary: {
    light: "#ff7961",
    main: "#2966ff",
    dark: "#0048fd",
    contrastText: "#fff",
  },
  secondary: {
    light: "#c063f5",
    main: "#b22bff",
    dark: "#9f10f1",
    contrastText: "#fff",
  },
};

export const appTheme = createMuiTheme({
  palette,
});

// General
export const useStylesGeneral = makeStyles((theme) => ({
  root: {
    background: theme.palette.grey,
    marginTop: 20,
    padding: 0,
  },
  card: {
    width: 320,
    maxWidth: 400,
    boxSizing: "border-box",
    margin: "auto",
    padding: 20,
  },
  form: {
    width: "100%",
    "& .MuiTextField-root": {
      width: "100%",
    },
    "& .MuiButton-containedPrimary": {
      float: "right",
    },
    "& .MuiFormHelperText-root": {
      color: "#ff0000",
    },
  },
  test: {
    width: "100%",
  },
}));

// Buttons
export const useStylesButtons = makeStyles((theme) => ({
  linkButton: {
    textDecoration: "none",
  },
}));

// Header
export const useStylesHeader = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  appBar: {
    background: theme.palette.primary.contrastText,
  },
  title: {
    flexGrow: 1,
    color: theme.palette.primary.main,
  },
  logo: {
    maxWidth: 50,
  },
}));

// Data grid
export const useStylesGrid = makeStyles((theme) => ({
  grid: {
    backgroundColor: theme.palette.primary.contrastText,
    height: 600,
    width: "100%",
    marginTop: 20,
    "& .MuiDataGrid-windowContainer, .MuiDataGrid-viewport": {
      minWidth: "700px !important",
      maxWidth: "100% !important",
      width: "100% !important",
    },
    "& .MuiDataGrid-window": {
      minWidth: 900,
    },
    "& .MuiDataGrid-columnsContainer, .MuiDataGrid-colCellWrapper ": {
      minWidth: 900,
      overflow: "visible",
    },
    "& .MuiDataGrid-main": {
      overflow: "scroll",
    },
  },
  leftGap: {
    marginLeft: 10,
  },
}));
