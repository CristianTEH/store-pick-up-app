import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Formik, Form } from "formik";
import {
  Container,
  Button,
  Box,
  Grid,
  Card,
  CardContent,
} from "@material-ui/core";
import { useStylesGeneral } from "./Common/Styles";
import { addOrder } from "../store/orders";
import * as Yup from "yup";
import Text from "./FormikField/Text";
import Number from "./FormikField/Number";
import DatePicker from "./FormikField/DatePicker";
import LinkButton from "./Common/LinkButton";
import { Redirect } from "react-router-dom";

const OrderForm = (props) => {
  const dispatch = useDispatch();
  const classes = useStylesGeneral();
  const orders = useSelector(addOrder);
  const [orderAddedd, setOrderAddedd] = useState(false);

  useEffect(() => {
    if (orders.payload.data.entities.orders.success) {
      setOrderAddedd(orders.payload.data.entities.orders.success);
    }
  }, [orders]);

  const initialValues = {
    name: "",
    content: "",
    weight: "",
    quantity: "",
    pickUpAddress: "",
    deliveryAddress: "",
    pickUpTime: "",
  };

  const OrderSchema = Yup.object().shape({
    name: Yup.string().required("Name content is required!"),
    content: Yup.string().required("Package content is required!"),
    weight: Yup.number().required("Weight is required!"),
    quantity: Yup.number().required("Quantity is required!"),
    pickUpAddress: Yup.string().required("Pick Up Address is required!"),
    deliveryAddress: Yup.string().required("Delivery Address is required!"),
    pickUpTime: Yup.string().test(
      "pickUpTime",
      "Pick up date can't be sooner then 24 hours from today!",
      (value) => {
        const nextDay = new Date();
        nextDay.setDate(new Date().getDate() + 1);
        return new Date(value).getTime() > nextDay.getTime();
      }
    ),
  });

  const handleSubmit = (values) => {
    dispatch(addOrder(values));
  };

  return (
    <>
      {orderAddedd ? (
        <Redirect to="/" />
      ) : (
        <Container className={classes.root}>
          <Card className={classes.card}>
            <CardContent>
              <Formik
                initialValues={initialValues}
                onSubmit={handleSubmit}
                validationSchema={OrderSchema}
              >
                {({ dirty, isValid }) => {
                  return (
                    <Grid container spacing={3}>
                      <Form className={classes.form}>
                        <Text
                          id="name"
                          name="name"
                          label="Name"
                          required={true}
                        />
                        <Text
                          id="content"
                          name="content"
                          label="Package Content"
                          required={true}
                        />
                        <Grid container spacing={3}>
                          <Grid item xs={6}>
                            <Number
                              id="weight"
                              name="weight"
                              min="0"
                              max="10"
                              label="Weight (kg)"
                              required={true}
                            />
                          </Grid>
                          <Grid item xs={6}>
                            <Number
                              id="quantity"
                              min="0"
                              max="100"
                              name="quantity"
                              label="quantity"
                              required={true}
                            />
                          </Grid>
                        </Grid>
                        <Text
                          id="pickUpAddress"
                          name="pickUpAddress"
                          label="Pick Up Address"
                          required={true}
                        />
                        <Text
                          id="deliveryAddress"
                          name="deliveryAddress"
                          label="Delivery Address"
                          required={true}
                        />
                        <DatePicker
                          id="pickUpTime"
                          name="pickUpTime"
                          label="Delivery Date"
                          required={true}
                        />
                        <Box width="100%" my={2}>
                          <Grid container spacing={3}>
                            <Grid item xs={6}>
                              <LinkButton
                                location="/"
                                text="Back"
                                color="secondary"
                              />
                            </Grid>
                            <Grid item xs={6}>
                              <Button
                                type="submit"
                                variant="contained"
                                color="primary"
                              >
                                Pick Up!
                              </Button>
                            </Grid>
                          </Grid>
                        </Box>
                      </Form>
                    </Grid>
                  );
                }}
              </Formik>
            </CardContent>
          </Card>
        </Container>
      )}
    </>
  );
};

export default OrderForm;
