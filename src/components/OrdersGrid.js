import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadOrders, getOrders } from "../store/orders";
import { Box, Grid } from "@material-ui/core";
import { DataGrid, GridToolbar, GridFooter } from "@material-ui/data-grid";
import { useStylesGrid } from "./Common/Styles";
import LinkButton from "./Common/LinkButton";

const OrdersGrid = () => {
  const dispatch = useDispatch();
  const orders = useSelector(getOrders);
  const classes = useStylesGrid();

  useEffect(() => {
    dispatch(loadOrders());
  }, [dispatch]);

  const columns = [
    { field: "id", headerName: "ID", type: "number", flex: 1, width: 300 },
    { field: "name", headerName: "Name", flex: 2, width: 300 },
    { field: "content", headerName: "Package Content", flex: 2, width: 300 },
    {
      field: "weight",
      headerName: "Weight(kg)",
      type: "number",
      flex: 1,
      width: 300,
    },
    {
      field: "quantity",
      headerName: "Quantity",
      type: "number",
      flex: 1,
      width: 300,
    },
    {
      field: "pickUpAddress",
      headerName: "Pick Up Address",
      flex: 2,
      width: 300,
    },
    {
      field: "deliveryAddress",
      headerName: "Delivery Address",
      flex: 2,
      width: 300,
    },
    {
      field: "pickUpTime",
      headerName: "Pick Up Time",
      type: "dateTime",
      flex: 2,
      width: 300,
    },
  ];

  const Footer = () => {
    return (
      <Box width="100%" my={2}>
        <Grid container spacing={3}>
          <Grid item xs={6}>
            <div className={classes.leftGap}>
              <LinkButton
                location="/order-submit"
                text="Add new pick up"
                color="primary"
              />
            </div>
          </Grid>
          <Grid item xs={6}>
            <GridFooter />
          </Grid>
        </Grid>
      </Box>
    );
  };

  return (
    <div className={classes.grid}>
      <DataGrid
        rows={orders}
        columns={columns}
        pageSize={8}
        components={{
          Toolbar: GridToolbar,
          Footer,
        }}
      />
    </div>
  );
};

export default OrdersGrid;
