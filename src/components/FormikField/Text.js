import React from "react";
import { ErrorMessage, Field } from "formik";
import TextField from "@material-ui/core/TextField";

function Text({ id, name, label, required = false }) {
  return (
    <div className="FormikField">
      <Field
        id={id}
        autoComplete="off"
        as={TextField}
        label={label}
        name={name}
        fullWidth={true}
        type={"text"}
        helperText={<ErrorMessage name={name} />}
      />
    </div>
  );
}

export default Text;
