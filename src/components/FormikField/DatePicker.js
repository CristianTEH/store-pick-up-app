import React from "react";
import TextField from "@material-ui/core/TextField";
import { ErrorMessage, Field } from "formik";

function DatePicker({ id, name, label, required = false }) {
  return (
    <Field
      id={id}
      label={label}
      name={name}
      as={TextField}
      type="datetime-local"
      helperText={<ErrorMessage name={name} />}
      InputLabelProps={{
        shrink: true,
      }}
    />
  );
}

export default DatePicker;
