import React from "react";
import { ErrorMessage, Field } from "formik";
import TextField from "@material-ui/core/TextField";

function Number({ id, name, label, min = 0, max = 100 }) {
  return (
    <div className="FormikField">
      <Field
        id={id}
        autoComplete="off"
        as={TextField}
        label={label}
        name={name}
        InputProps={{ inputProps: { min, max } }}
        fullWidth={false}
        type={"number"}
        helperText={<ErrorMessage name={name} />}
      />
    </div>
  );
}

export default Number;
