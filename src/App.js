import { Provider } from "react-redux";
import { Route, Switch } from "react-router-dom";
import configureStore from "./store/configureStore";
import OrdersGrid from "./components/OrdersGrid";
import OrderForm from "./components/OrderForm";
import Header from "./components/Common/Header";
import { MuiThemeProvider } from "@material-ui/core";
import { appTheme } from "./components/Common/Styles";

const store = configureStore();

function App() {
  return (
    <MuiThemeProvider theme={appTheme}>
      <Provider store={store}>
        <Header />
        <Switch>
          <Route path="/order-submit" component={OrderForm} />
          <Route path="/" component={OrdersGrid} />
        </Switch>
      </Provider>
    </MuiThemeProvider>
  );
}

export default App;
