const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const { customAlphabet } = require("nanoid");
const nanoid = customAlphabet("1234567890", 4);

app.use(cors());
app.use(bodyParser.json());

const orders = [
  {
    id: nanoid(),
    name: "Holli James",
    content: "Black Box",
    quantity: 1,
    weight: 3,
    pickUpAddress: "4493  Marie Street",
    pickUpTime: "2021-12-30T10:30",
    deliveryAddress: "3366  Angus Road",
  },
  {
    id: nanoid(),
    name: "Rayhaan Ortiz",
    content: "Red Box",
    quantity: 3,
    weight: 1,
    pickUpAddress: "4025  Whiteman Street",
    pickUpTime: "2021-01-31T20:30",
    deliveryAddress: "544  Whispering Pines Circle",
  },
  {
    id: nanoid(),
    name: "Tyriq Sumner",
    content: "Yellow Box",
    quantity: 2,
    weight: 4,
    pickUpAddress: "1369  Eden Drive",
    pickUpTime: "2021-03-02T11:30",
    deliveryAddress: "1963  Prospect Valley Road",
  },
  {
    id: nanoid(),
    name: "Graham Kendall",
    content: "Green Box",
    quantity: 5,
    weight: 2,
    pickUpAddress: "4547  Shadowmar Drive",
    pickUpTime: "2021-01-04T15:30",
    deliveryAddress: "543  Elk Rd Little",
  },
];

app.get("/api/orders", (req, res) => {
  res.json(orders);
});

app.post("/api/orders", (req, res) => {
  const order = { id: nanoid(), ...req.body };
  orders.push(order);

  res.json(order);
});

app.patch("/api/orders/:id", (req, res) => {
  const index = orders.findIndex(
    (order) => order.id === parseInt(req.params.id)
  );
  const order = orders[index];
  if ("delivered" in req.body) order.delivered = req.body.delivered;
  res.json(order);
});

app.listen(9001, () => {
  console.log("Node server started on port 9001.");
});
