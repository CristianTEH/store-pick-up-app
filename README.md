# PickApp #

* Demo app using react, redux and a basic node backend. 
* Version 0.1.0

### Install and start app ###

Make sure you have npm install.

* run **npm i** in root folder 
* run **npm start** to run the application
* it will run on localhost, port 3000

### Install and start backend for app ###

* run **npm i** in backend folder
* run **npm start** to start